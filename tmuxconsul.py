import argparse
import json
import os
import urllib.request

CONSUL_SERVICE_OPERATION = '/v1/catalog/service/'

ssh_user_name = os.getenv('USER')
consul_host = ''
consul_service_name = ''
tmux_attach_on_end = True


def parse_arguments():
    parser = argparse.ArgumentParser(description='Description for my parser', conflict_handler='resolve')

    parser.add_argument('-h', '-help', required=False, default='')
    parser.add_argument('-u', '-user', help='Example: -u yourname', required=False, default=os.getenv('USER'))
    parser.add_argument('-ccf', '-consul_config_file', help='Example: -ccf /path/to/json', required=False, default='')
    parser.add_argument('-ch', '-consul_host', help='Example: -ch http://consul.stage-hbg-aws-eu-west-1.service:8500',
                        required=False, default='')
    parser.add_argument('-cs', '-consul_service', help='Example: -cs your-service-name', required=True, default='')
    parser.add_argument('-tsn', '-tmux_session_name', help='Example: -tsn my-session', required=False, default='')
    parser.add_argument('-a', '-attached', help='Example: -a False', required=False, default=True)

    """ TODO: validation
    valid_argumentss = True
    if arguments.help:
        parser.print_help()
    else:
        if not arguments.consul_config_file and not arguments.consul_host:
            valid_argumentss = False
        if not arguments.consul_service:
            valid_argumentss = False
        if not valid_argumentss:
            print('Bad usage:')
            parser.print_help()
        if arguments.user:
            ssh_user_name = arguments.user
        if arguments.consul_config_file:
            print('-ccf not yet supported. It will be a json file mapping consul_host/s and consul_service/s to aliases')
        if arguments.consul_host:
            consul_host = arguments.consul_host
        if arguments.consul_service:
            consul_service_name = arguments.consul_service
        if arguments.tmux_session_name:
            tmux_session = arguments.tmux_session_name
        else:
            tmux_session = 'tmuxconsul-' + consul_service_name
        if arguments.attached:
            tmux_attach_on_end = arguments.attached"""

    arguments = parser.parse_args()
    return arguments


def send_ssh_command(address, tmux_session):
    ssh_command = 'ssh -o "StrictHostKeyChecking=no" ' + ssh_user_name + '@' + address
    send_command = 'tmux send-keys -t ' + tmux_session + ' "' + ssh_command + '" Enter'
    os.system(send_command)


if __name__ == "__main__":
    arguments = parse_arguments()
    if arguments.u:
        ssh_user_name = arguments.u
    consul_host = arguments.ch
    consul_service_name = arguments.cs
    tmux_session = 'tmuxconsul-' + consul_service_name

    with urllib.request.urlopen(consul_host + CONSUL_SERVICE_OPERATION + consul_service_name) as url:
        os.system('tmux new -d -s ' + tmux_session)
        nodes = url.read()
        nodes = json.loads(nodes)
        node_addresses = set([node['Address'] for node in nodes])
        send_ssh_command(node_addresses.pop(), tmux_session)
        for node in node_addresses:
            os.system('tmux split -t ' + tmux_session)
            os.system('tmux select-layout -t ' + tmux_session + ' tiled')
            send_ssh_command(node, tmux_session)
    os.system('tmux set-window-option -t ' + tmux_session + ' synchronize-panes on')
    if tmux_attach_on_end:
        os.system('tmux attach-session -t ' + tmux_session)
