# tmux-consul

tmux-consul is a Python generator of tmux sessions from Consul API data

## Requirements
This tool requires:
- [Python2](https://www.python.org/downloads/release/python-272/)
- [tmux](https://github.com/tmux/tmux/wiki), the most flexible and standard [terminal multiplexer](https://en.wikipedia.org/wiki/Terminal_multiplexer)

Since Python2 is already installed on almost every Linux distribution, real requirements left is tmux; to install it on:

- ### ArchLinux based distributions like Manjaro
```bash
sudo pacman -S tmux
```
- ### Debian based distributions like Ubuntu
```bash
sudo apt install tmux
```
- ### CentOS and Fedora
```bash
sudo yum install tmux
```
## Usage

```bash
python tmuxconsul.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)